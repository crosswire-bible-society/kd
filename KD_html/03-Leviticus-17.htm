<html>

<script language="JavaScript" src="bwjs.js"></script>
<head>
<meta http-equiv=Content-Type content="text/html; charset=windows-1252">
<meta name=Generator content="Microsoft Word 11 (filtered)">
<title>Leviticus 17</title>
<style>
<!--
 /* Font Definitions */
 @font-face
	{font-family:Georgia;
	panose-1:2 4 5 2 5 4 5 2 3 3;}
@font-face
	{font-family:"TITUS Cyberbit Basic";
	panose-1:2 2 6 3 5 4 5 2 3 4;}
 /* Style Definitions */
 p.MsoNormal, li.MsoNormal, div.MsoNormal
	{margin:0cm;
	margin-bottom:.0001pt;
	font-size:12.0pt;
	font-family:"Times New Roman";}
 /* Page Definitions */
 @page Section1
	{size:612.0pt 792.0pt;
	margin:72.0pt 90.0pt 72.0pt 90.0pt;}
div.Section1
	{page:Section1;}
-->
</style>

</head>

<body lang=AF style='text-justify-trim:punctuation' onkeypress="BwOnKeyPress()" onload="BwOnLoad()">

<div class=Section1>

<p class=MsoNormal align=center style='margin-top:4.0pt;margin-right:0cm;
margin-bottom:2.0pt;margin-left:0cm;text-align:center;text-autospace:none'><b><span
style='font-family:Georgia'>II. Laws for the Sanctification of Israel in the
Covenant - Fellowship of Its God - Leviticus 17-25</span></b></p>

<p class=MsoNormal align=center style='margin-top:4.0pt;margin-right:0cm;
margin-bottom:2.0pt;margin-left:0cm;text-align:center;text-autospace:none'><b><span
style='font-family:Georgia'>Holiness of Conduct on the Part of the Israelites -
Leviticus 17-20</span></b></p>

<p class=MsoNormal style='margin-bottom:3.0pt;text-indent:10.8pt;text-autospace:
none'><span style='font-family:Georgia'>&nbsp;</span></p>

<p class=MsoNormal style='margin-bottom:3.0pt;text-indent:10.8pt;text-autospace:
none'><span style='font-family:Georgia'>The contents of these four chapters
have been very fittingly summed up by Baumgarten in the following heading:
�Israel is not to walk in the way of the heathen and of the Canaanites, but in
the ordinances of Jehovah,� as all the commandments contained in them relate to
holiness of life.</span></p>

<p class=MsoNormal style='margin-bottom:3.0pt;text-indent:10.8pt;text-autospace:
none'><span style='font-family:Georgia'>Holiness of Food. - The Israelites were
not to slaughter domestic animals as food either within or outside the camp,
but before the door of the tabernacle, and as slain-offerings, that the blood
and fat might be offered to Jehovah. They were not to sacrifice any more to
field-devils (<a name="ref1" href="javascript:BwRef('Lev 17:3-7')" onMouseOut="javascript:BwUnPop()" onMouseOver="javascript:BwPop('Lev 17:3-7')">Lev 17:3-7</a>), and were to offer all their burnt-offerings or
slain-offerings before the door of the tabernacle (<a name="ref2" href="javascript:BwRef('Lev 17:8')" onMouseOut="javascript:BwUnPop()" onMouseOver="javascript:BwPop('Lev 17:8')">Lev 17:8</a>,� 9); and they were
not to eat either blood or carrion (<a name="ref3" href="javascript:BwRef('Lev 17:10-16')" onMouseOut="javascript:BwUnPop()" onMouseOver="javascript:BwPop('Lev 17:10-16')">Lev 17:10-16</a>). These laws are not intended
simply as supplements to the food laws in ch. 11; but they place the eating of
food on the part of the Israelites in the closest relation with their calling
as the holy nation of Jehovah, on the one hand to oppose an effectual barrier
to the inclination of the people to idolatrous sacrificial meals, on the other
hand to give a consecrated character to the food of the people in harmony with
their calling, that it might be received with thanksgiving and sanctified with
prayer (<a name="ref4" href="javascript:BwRef('1Ti 4:4-5')" onMouseOut="javascript:BwUnPop()" onMouseOver="javascript:BwPop('1Ti 4:4-5')">1Ti 4:4-5</a>).</span></p>

<p class=MsoNormal style='margin-bottom:3.0pt;text-indent:10.8pt;text-autospace:
none'><span style='font-family:Georgia'>&nbsp;</span></p>

<p class=MsoNormal style='margin-bottom:3.0pt;text-autospace:none'><b><span
style='font-family:Georgia'><a name="ref5" href="javascript:BwRef('Lev 17:1-2')" onMouseOut="javascript:BwUnPop()" onMouseOver="javascript:BwPop('Lev 17:1-2')">Lev 17:1-2</a></span></b></p>

<p class=MsoNormal style='margin-bottom:3.0pt;text-indent:10.8pt;text-autospace:
none'><span style='font-family:Georgia'>The directions are given to �Aaron and
his sons, and all the children of Israel,� because they were not only binding
upon the nation generally, but upon the priesthood also; whereas the
instructions in ch. 18-20 are addressed to �the children of Israel,� or �the
whole congregation� (<a name="ref6" href="javascript:BwRef('Lev 18:2')" onMouseOut="javascript:BwUnPop()" onMouseOver="javascript:BwPop('Lev 18:2')">Lev 18:2</a>; <a name="ref7" href="javascript:BwRef('Lev 19:2')" onMouseOut="javascript:BwUnPop()" onMouseOver="javascript:BwPop('Lev 19:2')">Lev 19:2</a>; <a name="ref8" href="javascript:BwRef('Lev 20:2')" onMouseOut="javascript:BwUnPop()" onMouseOver="javascript:BwPop('Lev 20:2')">Lev 20:2</a>), just as special laws are
laid down for the priests in ch. 20 and 21 with reference to the circumstances
mentioned there.</span></p>

<p class=MsoNormal style='margin-bottom:3.0pt;text-indent:10.8pt;text-autospace:
none'><span style='font-family:Georgia'>&nbsp;</span></p>

<p class=MsoNormal style='margin-bottom:3.0pt;text-autospace:none'><b><span
style='font-family:Georgia'><a name="ref9" href="javascript:BwRef('Lev 17:3-7')" onMouseOut="javascript:BwUnPop()" onMouseOver="javascript:BwPop('Lev 17:3-7')">Lev 17:3-7</a></span></b></p>

<p class=MsoNormal style='margin-bottom:3.0pt;text-indent:10.8pt;text-autospace:
none'><span style='font-family:Georgia'>Whoever of the house of Israel
slaughtered an ox, sheep, or goat, either within or outside the camp, without
bringing the animal to the tabernacle, to offer a sacrifice therefrom to the
Lord, �<i>blood was to be reckoned to him;</i>� that is to say, as the
following expression, �he hath shed blood,� shows, such slaughtering was to be
reckoned as the shedding of blood, or blood-guiltiness, and punished with
extermination (see <a name="ref10" href="javascript:BwRef('Gen 17:14')" onMouseOut="javascript:BwUnPop()" onMouseOver="javascript:BwPop('Gen 17:14')">Gen 17:14</a>). The severity of this prohibition required some
explanation, and this is given in the reason assigned in <a name="ref11" href="javascript:BwRef('Lev 17:5-7')" onMouseOut="javascript:BwUnPop()" onMouseOver="javascript:BwPop('Lev 17:5-7')">Lev 17:5-7</a>, viz.,
�that the Israelites may bring their slain-offerings, which they slay in the
open field, before the door of the tabernacle, as peace-offerings to Jehovah,�
and �no more offer their sacrifices to the </span><span style='font-family:
"TITUS Cyberbit Basic"'>&#1513;&#1474;&#1506;&#1497;&#1512;&#1497;&#1501;</span><span
style='font-family:Georgia'>, after whom they go a whoring� (<a name="ref12" href="javascript:BwRef('Lev 17:7')" onMouseOut="javascript:BwUnPop()" onMouseOver="javascript:BwPop('Lev 17:7')">Lev 17:7</a>). This
reason presupposes that the custom of dedicating the slain animals as
sacrifices to some deity, to which a portion of them was offered, was then
widely spread among the Israelites. It had probably been adopted from the
Egyptians; though this is not expressly stated by ancient writers: <i>Herodotus</i>
(i. 132) and <i>Strabo</i> (xv. 732) simply mentioning it as a Persian custom,
whilst the law book of Manu ascribes it to the Indians. To root out this
idolatrous custom from among the Israelites, they were commanded to slay every
animal before the tabernacle, as a sacrificial gift to Jehovah, and to bring
the slain-offerings, which they would have slain in the open field, to the
priest at the tabernacle, as </span><i><span style='font-family:"TITUS Cyberbit Basic"'>shelamim</span></i><span
style='font-family:Georgia'> (praise-offerings and thank-offerings), that he
might sprinkle the blood upon the altar, and burn the fat as a sweet-smelling
savour for Jehovah (see <a name="ref13" href="javascript:BwRef('Lev 3:2-5')" onMouseOut="javascript:BwUnPop()" onMouseOver="javascript:BwPop('Lev 3:2-5')">Lev 3:2-5</a>). �<i>The face of the field</i>� (<a name="ref14" href="javascript:BwRef('Lev 17:5')" onMouseOut="javascript:BwUnPop()" onMouseOver="javascript:BwPop('Lev 17:5')">Lev 17:5</a>,
as in <a name="ref15" href="javascript:BwRef('Lev 14:7')" onMouseOut="javascript:BwUnPop()" onMouseOver="javascript:BwPop('Lev 14:7')">Lev 14:7</a>, <a name="ref16" href="javascript:BwRef('Lev 14:53')" onMouseOut="javascript:BwUnPop()" onMouseOver="javascript:BwPop('Lev 14:53')">Lev 14:53</a>): the open field, in distinction from the enclosed
space of the court of Jehovah's dwelling. �The altar of Jehovah� is spoken of
in <a name="ref17" href="javascript:BwRef('Lev 17:6')" onMouseOut="javascript:BwUnPop()" onMouseOver="javascript:BwPop('Lev 17:6')">Lev 17:6</a> instead of �<i>the altar</i>� only (<a name="ref18" href="javascript:BwRef('Lev 1:5')" onMouseOut="javascript:BwUnPop()" onMouseOver="javascript:BwPop('Lev 1:5')">Lev 1:5</a>; <a name="ref19" href="javascript:BwRef('Lev 11:15')" onMouseOut="javascript:BwUnPop()" onMouseOver="javascript:BwPop('Lev 11:15')">Lev 11:15</a>, etc.), on
account of the contrast drawn between it and the altars upon which they offered
sacrifice to <i>Seirim</i>. </span><span style='font-family:"TITUS Cyberbit Basic"'>&#1513;&#1474;&#1506;&#1497;&#1512;&#1497;&#1501;</span><span
style='font-family:Georgia'>, literally goats, is here used to signify <i>daemones</i>
(<i>Vulg</i>.), �field-devils� (<i>Luther</i>), demons, like the </span><span
style='font-family:"TITUS Cyberbit Basic"'>&#1513;&#1474;&#1491;&#1497;&#1501;</span><span
style='font-family:Georgia'> in <a name="ref20" href="javascript:BwRef('Deu 32:17')" onMouseOut="javascript:BwUnPop()" onMouseOver="javascript:BwPop('Deu 32:17')">Deu 32:17</a>, who were supposed to inhabit the
desert (<a name="ref21" href="javascript:BwRef('Isa 13:21')" onMouseOut="javascript:BwUnPop()" onMouseOver="javascript:BwPop('Isa 13:21')">Isa 13:21</a>; <a name="ref22" href="javascript:BwRef('Isa 34:14')" onMouseOut="javascript:BwUnPop()" onMouseOver="javascript:BwPop('Isa 34:14')">Isa 34:14</a>), and whose pernicious influence they sought to
avert by sacrifices. The Israelites had brought this superstition, and the
idolatry to which it gave rise, from Egypt. The <i>Seirim</i> were the gods
whom the Israelites worshipped and went a whoring after in Egypt (<a name="ref23" href="javascript:BwRef('Jos 24:14')" onMouseOut="javascript:BwUnPop()" onMouseOver="javascript:BwPop('Jos 24:14')">Jos 24:14</a>;
<a name="ref24" href="javascript:BwRef('Eze 20:7')" onMouseOut="javascript:BwUnPop()" onMouseOver="javascript:BwPop('Eze 20:7')">Eze 20:7</a>; <a name="ref25" href="javascript:BwRef('Eze 23:3')" onMouseOut="javascript:BwUnPop()" onMouseOver="javascript:BwPop('Eze 23:3')">Eze 23:3</a>, <a name="ref26" href="javascript:BwRef('Eze 23:8')" onMouseOut="javascript:BwUnPop()" onMouseOver="javascript:BwPop('Eze 23:8')">Eze 23:8</a>, <a name="ref27" href="javascript:BwRef('Eze 23:19')" onMouseOut="javascript:BwUnPop()" onMouseOver="javascript:BwPop('Eze 23:19')">Eze 23:19</a>, <a name="ref28" href="javascript:BwRef('Eze 23:21')" onMouseOut="javascript:BwUnPop()" onMouseOver="javascript:BwPop('Eze 23:21')">Eze 23:21</a>, <a name="ref29" href="javascript:BwRef('Eze 23:27')" onMouseOut="javascript:BwUnPop()" onMouseOver="javascript:BwPop('Eze 23:27')">Eze 23:27</a>). Both the thing
and the name were derived from the Egyptians, who worshipped goats as gods
(Josephus c. Ap<i>. </i>2, 7), particularly <i>Pan</i>, who was represented in
the form of a goat, a personification of the male and fertilizing principle in
nature, whom they called <i>Mendes</i> and reckoned among the eight leading
gods, and to whom they had built a splendid and celebrated temple in <i>Thmuis</i>,
the capital of the Mendesian <i>Nomos</i> in Lower Egypt, and erected statues
in the temples in all directions (cf. <i>Herod</i>. 2, 42, 46; <i>Strabo</i>,
xvii. 802; <i>Diod. Sic.</i> i. 18). The expression �a statute for ever� refers
to the principle of the law, that sacrifices were to be offered to Jehovah
alone, and not to the law that every animal was to be slain before the
tabernacle, which was afterwards repealed by Moses, when they were about to
enter Canaan, where it could no longer be carried out (<a name="ref30" href="javascript:BwRef('Deu 12:15')" onMouseOut="javascript:BwUnPop()" onMouseOver="javascript:BwPop('Deu 12:15')">Deu 12:15</a>).</span></p>

<p class=MsoNormal style='margin-bottom:3.0pt;text-indent:10.8pt;text-autospace:
none'><span style='font-family:Georgia'>&nbsp;</span></p>

<p class=MsoNormal style='margin-bottom:3.0pt;text-autospace:none'><b><span
style='font-family:Georgia'><a name="ref31" href="javascript:BwRef('Lev 17:8-16')" onMouseOut="javascript:BwUnPop()" onMouseOver="javascript:BwPop('Lev 17:8-16')">Lev 17:8-16</a></span></b></p>

<p class=MsoNormal style='margin-bottom:3.0pt;text-indent:10.8pt;text-autospace:
none'><span style='font-family:Georgia'>To this there are appended three laws,
which are kindred in their nature, and which were binding not only upon the
Israelites, but also upon the foreigners who dwelt in the midst of them.</span></p>

<p class=MsoNormal style='margin-top:4.0pt;margin-right:0cm;margin-bottom:2.0pt;
margin-left:0cm;text-autospace:none'><b><span style='font-family:Georgia'>&nbsp;</span></b></p>

<p class=MsoNormal style='margin-top:4.0pt;margin-right:0cm;margin-bottom:2.0pt;
margin-left:0cm;text-autospace:none'><b><span style='font-family:Georgia'><a name="ref32" href="javascript:BwRef('Lev 17:8-12')" onMouseOut="javascript:BwUnPop()" onMouseOver="javascript:BwPop('Lev 17:8-12')">Lev
17:8-12</a></span></b></p>

<p class=MsoNormal style='margin-bottom:3.0pt;text-indent:10.8pt;text-autospace:
none'><span style='font-family:Georgia'><a name="ref33" href="javascript:BwRef('Lev 17:8')" onMouseOut="javascript:BwUnPop()" onMouseOver="javascript:BwPop('Lev 17:8')">Lev 17:8</a>, <a name="ref34" href="javascript:BwRef('Lev 17:9')" onMouseOut="javascript:BwUnPop()" onMouseOver="javascript:BwPop('Lev 17:9')">Lev 17:9</a> contain the command,
that whoever offered a burnt-offering of slain-offering, and did not bring it
to the tabernacle to prepare it for Jehovah there, was to be exterminated; a
command which involved the prohibition of sacrifice in any other place whatever,
and was given, as the further extension of this law in <a name="ref35" href="javascript:BwRef('Deu 12')" onMouseOut="javascript:BwUnPop()" onMouseOver="javascript:BwPop('Deu 12')">Deut 12</a> clearly proves,
for the purpose of suppressing the disposition to offer sacrifice to other
gods, as well as in other places. In <a name="ref36" href="javascript:BwRef('Lev 17:10-14')" onMouseOut="javascript:BwUnPop()" onMouseOver="javascript:BwPop('Lev 17:10-14')">Lev 17:10-14</a> the prohibition of the eating
of blood is repeated, and ordered to be observed on pain of extermination; it
is also extended to the strangers in Israel; and after a more precise
explanation of the reason for the law, is supplemented by instructions for the
disposal of the blood of edible game. God threatens that He will inflict the
punishment Himself, because the eating of blood was a transgression of the law
which might easily escape the notice of the authorities. �To set one's face
against:� i.e., to judge. The reason for the command in <a name="ref37" href="javascript:BwRef('Lev 17:11')" onMouseOut="javascript:BwUnPop()" onMouseOver="javascript:BwPop('Lev 17:11')">Lev 17:11</a>, �For the
soul of the flesh (the soul which gives life to the flesh) is in the blood, and
I have given it to you upon the altar, to make an atonement for your souls,� is
not a double one, viz., (1) because the blood contained the soul of the animal,
and (2) because God had set apart the blood, as the medium of expiation for the
human soul, for the altar, i.e., to be sprinkled upon the altar. The first
reason simply forms the foundation for the second: God appointed the blood for
the altar, as containing the soul of the animal, to be the medium of expiation
for the souls of men, and therefore prohibited its being used as food. �For the
blood it expiates <i>by virtue</i> of the soul,� not �the soul� itself. </span><span
style='font-family:"TITUS Cyberbit Basic"'>&#1489;&#1468;</span><span
style='font-family:Georgia'> with </span><span style='font-family:"TITUS Cyberbit Basic"'>&#1499;&#1468;&#1508;&#1468;&#1512;</span><span
style='font-family:Georgia'> has only a local or instrumental signification
(<a name="ref38" href="javascript:BwRef('Lev 6:23')" onMouseOut="javascript:BwUnPop()" onMouseOver="javascript:BwPop('Lev 6:23')">Lev 6:23</a>; <a name="ref39" href="javascript:BwRef('Lev 16:17')" onMouseOut="javascript:BwUnPop()" onMouseOver="javascript:BwPop('Lev 16:17')">Lev 16:17</a>, <a name="ref40" href="javascript:BwRef('Lev 16:27')" onMouseOut="javascript:BwUnPop()" onMouseOver="javascript:BwPop('Lev 16:27')">Lev 16:27</a>; also <a name="ref41" href="javascript:BwRef('Lev 7:7')" onMouseOut="javascript:BwUnPop()" onMouseOver="javascript:BwPop('Lev 7:7')">Lev 7:7</a>; <a name="ref42" href="javascript:BwRef('Exo 29:33')" onMouseOut="javascript:BwUnPop()" onMouseOver="javascript:BwPop('Exo 29:33')">Exo 29:33</a>; <a name="ref43" href="javascript:BwRef('Num 5:8')" onMouseOut="javascript:BwUnPop()" onMouseOver="javascript:BwPop('Num 5:8')">Num 5:8</a>).
Accordingly, it was not the blood as such, but the blood as the vehicle of the
soul, which possessed expiatory virtue; because the animal soul was offered to
God upon the altar as a substitute for the human soul. Hence every bleeding
sacrifice had an expiatory force, though without being an expiatory sacrifice
in the strict sense of the word.</span></p>

<p class=MsoNormal style='margin-top:4.0pt;margin-right:0cm;margin-bottom:2.0pt;
margin-left:0cm;text-autospace:none'><b><span style='font-family:Georgia'>&nbsp;</span></b></p>

<p class=MsoNormal style='margin-top:4.0pt;margin-right:0cm;margin-bottom:2.0pt;
margin-left:0cm;text-autospace:none'><b><span style='font-family:Georgia'><a name="ref44" href="javascript:BwRef('Lev 17:13')" onMouseOut="javascript:BwUnPop()" onMouseOver="javascript:BwPop('Lev 17:13')">Lev
17:13</a></span></b></p>

<p class=MsoNormal style='margin-bottom:3.0pt;text-indent:10.8pt;text-autospace:
none'><span style='font-family:Georgia'>The blood also of such hunted game as
was edible, whether bird or beast, was not to be eaten either by the Israelite
or stranger, but to be poured out and covered with earth. In <a name="ref45" href="javascript:BwRef('Deu 12:16')" onMouseOut="javascript:BwUnPop()" onMouseOver="javascript:BwPop('Deu 12:16')">Deu 12:16</a> and <a name="ref46" href="javascript:BwRef('Deu 12:24')" onMouseOut="javascript:BwUnPop()" onMouseOver="javascript:BwPop('Deu 12:24')">Deu
12:24</a>, where the command to slay all the domestic animals at the tabernacle as
slain-offerings is repealed, this is extended to such domestic animals as were
slaughtered for food; their blood also was not to be eaten, but to be poured
upon the earth �like water,� i.e., not <i>quasi rem profanam et nullo ritu
sacro</i> (<i>Rosenm�ller</i>, etc.), but like water which is poured upon the
earth, sucked in by it, and thus given back to the womb of the earth, from
which God had caused the animals to come forth at their creation (<a name="ref47" href="javascript:BwRef('Gen 1:24')" onMouseOut="javascript:BwUnPop()" onMouseOver="javascript:BwPop('Gen 1:24')">Gen 1:24</a>).
Hence pouring it out upon the earth like water was substantially the same as
pouring it out and covering it with earth (cf. <a name="ref48" href="javascript:BwRef('Eze 24:7-8')" onMouseOut="javascript:BwUnPop()" onMouseOver="javascript:BwPop('Eze 24:7-8')">Eze 24:7-8</a>); and the purpose of
the command was to prevent the desecration of the vehicle of the soulish life,
which was sanctified as the medium of expiation.</span></p>

<p class=MsoNormal style='margin-top:4.0pt;margin-right:0cm;margin-bottom:2.0pt;
margin-left:0cm;text-autospace:none'><b><span style='font-family:Georgia'>&nbsp;</span></b></p>

<p class=MsoNormal style='margin-top:4.0pt;margin-right:0cm;margin-bottom:2.0pt;
margin-left:0cm;text-autospace:none'><b><span style='font-family:Georgia'><a name="ref49" href="javascript:BwRef('Lev 17:14-16')" onMouseOut="javascript:BwUnPop()" onMouseOver="javascript:BwPop('Lev 17:14-16')">Lev
17:14-16</a></span></b></p>

<p class=MsoNormal style='margin-bottom:3.0pt;text-indent:10.8pt;text-autospace:
none'><span style='font-family:Georgia'>�<i>For as for the soul of all
flesh...its blood makes out its soul:</i>� i.e., �this is the case with the
soul of all flesh, that it is its blood which makes out its soul.� </span><span
style='font-family:"TITUS Cyberbit Basic"'>&#1489;&#1468;&#1504;&#1508;&#1513;&#1473;&#1493;</span><span
style='font-family:Georgia'> is to be taken as a predicate in its meaning,
introduced with <i>beth essentiale</i>. It is only as so understood, that the
clause supplies a reason at all in harmony with the context. Because the
distinguishing characteristic of the blood as, that it was the soul of the
being when living in the flesh; therefore it was not to be eaten in the case of
any animal: and even in the case of animals that were not proper for sacrifice,
it was to be allowed to run out upon the ground, and then covered with earth,
or, so to speak, buried.</span></p>

<p class=MsoNormal style='margin-top:0cm;margin-right:0cm;margin-bottom:3.0pt;
margin-left:18.0pt;text-indent:10.8pt;text-autospace:none'><span
style='font-family:Georgia'>(Note: On the truth which lay at the foundation of
this idea of the unity of the soul and blood, which others of the ancients shared
with the Hebrews, particularly the early Greek philosophers, see <i>Delitzsch</i>'s
<i>bibl. Psychol.</i> pp. 242ff. �It seems at first sight to be founded upon no
other reason, than that a sudden diminution of the quantity of the blood is
sure to cause death. But this phenomenon rests upon the still deeper ground,
that all the activity of the body, especially that of the nervous and muscular
systems, is dependent upon the circulation of the blood; for if the flow of
blood is stopped from any part of the body, all its activity ceases
immediately; a sensitive part loses all sensation in a very few minutes, and
muscular action is entirely suspended... The blood is really the basis of the
physical life; and so far the soul, as the vital principle of the body, is
pre-eminently in the blood� (p. 245).)</span></p>

<p class=MsoNormal style='margin-bottom:3.0pt;text-autospace:none'><span
style='font-family:Georgia'>�- Lastly (<a name="ref50" href="javascript:BwRef('Lev 17:15')" onMouseOut="javascript:BwUnPop()" onMouseOver="javascript:BwPop('Lev 17:15')">Lev 17:15</a>, <a name="ref51" href="javascript:BwRef('Lev 17:16')" onMouseOut="javascript:BwUnPop()" onMouseOver="javascript:BwPop('Lev 17:16')">Lev 17:16</a>), the prohibition
against eating �that which died� (<a name="ref52" href="javascript:BwRef('Lev 11:39-40')" onMouseOut="javascript:BwUnPop()" onMouseOver="javascript:BwPop('Lev 11:39-40')">Lev 11:39-40</a>), or �that which was torn� (<a name="ref53" href="javascript:BwRef('Exo 22:30')" onMouseOut="javascript:BwUnPop()" onMouseOver="javascript:BwPop('Exo 22:30')">Exo
22:30</a>), is renewed and supplemented by the law, that whoever, either of the
natives or of foreigners, should eat the flesh of that which had fallen (died a
natural death), or had been torn in pieces by wild beasts (sc., thoughtlessly
or in ignorance; cf. <a name="ref54" href="javascript:BwRef('Lev 5:2')" onMouseOut="javascript:BwUnPop()" onMouseOver="javascript:BwPop('Lev 5:2')">Lev 5:2</a>), and neglected the legal purification afterwards,
was to bear his iniquity (<a name="ref55" href="javascript:BwRef('Lev 5:1')" onMouseOut="javascript:BwUnPop()" onMouseOver="javascript:BwPop('Lev 5:1')">Lev 5:1</a>). Of course the flesh intended is that of
animals which were clean, and therefore allowable as food, when properly
slaughtered, and which became unclean simply from the fact, that when they had
died a natural death, or had been torn to pieces by wild beasts, the blood
remained in the flesh, or did not flow out in a proper manner. According to <a name="ref56" href="javascript:BwRef('Exo 22:30')" onMouseOut="javascript:BwUnPop()" onMouseOver="javascript:BwPop('Exo 22:30')">Exo
22:30</a>, the </span><span style='font-family:"TITUS Cyberbit Basic"'>&#1504;&#1489;&#1500;&#1492;</span><span
style='font-family:Georgia'> (that which had fallen) was to be thrown to the
dogs; but in <a name="ref57" href="javascript:BwRef('Deu 14:21')" onMouseOut="javascript:BwUnPop()" onMouseOver="javascript:BwPop('Deu 14:21')">Deu 14:21</a> permission is given either to sell it or give it to a
stranger or alien, to prevent the plea that it was a pity that such a thing
should be entirely wasted, and so the more effectually to secure the observance
of the command, that it was not to be eaten by an Israelite.</span></p>

<p class=MsoNormal style='margin-bottom:3.0pt;text-autospace:none'><span
style='font-family:Georgia'>&nbsp;</span></p>

</div>

</body>

</html>
